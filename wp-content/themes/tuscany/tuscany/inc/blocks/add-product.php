<?php global $tuscany_opt;
	$price          = $tuscany_opt['tuscany-add-price'];
	$description    = $tuscany_opt['tuscany-add-description'];
	$product_img    = $tuscany_opt['tuscany-add-img']['url'];
	$additional_img = $tuscany_opt['tuscany-add-additional']['url'];
?>

<!-- Addvertisement Item -->
<!-- Collage block -->
<div class="entry-content"><div class="row row-edge">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 collapse-col">
<div class="portfolio-wrap portfolio_1"><div class="portfolio-container justified-layout loading-animation-move-up" data-post-per-page="5" data-column="3" data-list-layout="justified-layout" data-loading-mode="ajax-pagination" data-item-margin="0" data-fixed-height="300" data-template="portfolio_1.tmpl" data-posts-format="post-format-all"><div class="portfolio-list" data-all-posts-count="12"><div id="quote-97" class="portfolio-item item-1 odd  justified-layout-item simple-scale-hover clearfix" data-image-src="http://goodlunch2-d4sn9o65.um123.org/Block/1.jpg" data-image-width="500" data-image-height="350" data-image-ratio="1.4285714285714" style="width: 380px; height: 300px; flex: 143 1 380px; margin: 0px;"><div class="inner-wrap" style="background-image: url(http://goodlunch2-d4sn9o65.um123.org/Block/1.jpg);">
	<div class="item-content rollover-content">
		<div class="item-content-inner table-display">
			<div class="table-cell">
				<h3><a href="/art-kulinariya/">Арт-Кулинария</a></h3>
			</div>
		</div>
	</div>
</div></div><div id="quote-96" class="portfolio-item item-2 even  justified-layout-item simple-scale-hover clearfix" data-image-src="http://goodlunch2-d4sn9o65.um123.org/Block/2.jpg" data-image-width="500" data-image-height="350" data-image-ratio="1.4285714285714" style="width: 380px; height: 300px; flex: 143 1 380px; margin: 0px;"><div class="inner-wrap" style="background-image: url(http://goodlunch2-d4sn9o65.um123.org/Block/2.jpg);">
	<div class="item-content rollover-content">
		<div class="item-content-inner table-display">
			<div class="table-cell">
				<h3><a href="/category/catering/">Кейтеринг</a></h3>
			</div>
		</div>
	</div>
</div></div><div id="quote-95" class="portfolio-item item-3 odd  justified-layout-item simple-scale-hover clearfix" data-image-src="http://goodlunch2-d4sn9o65.um123.org/Block/3.jpg" data-image-width="500" data-image-height="350" data-image-ratio="1.4285714285714" style="width: 380px; height: 300px; flex: 143 1 380px; margin: 0px;"><div class="inner-wrap" style="background-image: url(http://goodlunch2-d4sn9o65.um123.org/Block/3.jpg);">
	<div class="item-content rollover-content">
		<div class="item-content-inner table-display">
			<div class="table-cell">
				<h3><a href="/shop-ecwid/">Заказ праздничных <br>блюд домой и в офис</a></h3>
			</div>
		</div>
	</div>
</div></div><div id="quote-94" class="portfolio-item item-4 even  justified-layout-item simple-scale-hover clearfix" data-image-src="http://goodlunch2-d4sn9o65.um123.org/Block/4.jpg" data-image-width="657" data-image-height="350" data-image-ratio="1.8771428571429" style="width: 570px; height: 300px; flex: 188 1 570px; margin: 0px;"><div class="inner-wrap" style="background-image: url(http://goodlunch2-d4sn9o65.um123.org/Block/4.jpg);">
	<div class="item-content rollover-content">
		<div class="item-content-inner table-display">
			<div class="table-cell">
				<h3><a href="/svadbi-banketi/">Свадьбы, банкеты</a></h3>
			</div>
		</div>
	</div>
</div></div><div id="quote-76" class="portfolio-item item-5 odd  justified-layout-item simple-scale-hover clearfix" data-image-src="http://goodlunch2-d4sn9o65.um123.org/Block/5.jpg" data-image-width="657" data-image-height="350" data-image-ratio="1.8771428571429" style="width: 570px; height: 300px; flex: 188 1 570px; margin: 0px;"><div class="inner-wrap" style="background-image: url(http://goodlunch2-d4sn9o65.um123.org/Block/5.jpg);">
	<div class="item-content rollover-content">
		<div class="item-content-inner table-display">
			<div class="table-cell">
				<h3><a href="/category/detskie-prazdniki/">Детские дни рождения в кафе</a></h3>
			</div>
		</div>
	</div>
</div></div>

<div id="quote-78" class="portfolio-item item-5 odd justified-layout-item simple-scale-hover clearfix" data-image-src="/Block/6.jpg" data-image-width="1140" data-image-height="3140" data-image-ratio="1.8771428571429" style="width: 1140px; height:314px; flex: 188 1 570px; margin: 0px;"><div class="inner-wrap" style="background-image: url(/Block/6.jpg);">
	<div class="item-content rollover-content">
		<div class="item-content-inner table-display">
			<div class="table-cell">
				<h3><a href="/category/gotovie-bluda/">Готовые полезные блюда каждый день</a></h3>
			</div>
		</div>
	</div>
</div></div></div>

</div>
</div>

<div class="row row-edge">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 collapse-col">

</div>
</div>
<!-- Collage block End -->

<div class="container top-divide">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="add-banner">
            <div class="additional-border"></div>
            <?php if (!empty($additional_img)): ?>
            	<div class="text-add cell-section hidden-xs hidden-sm element-animate" data-animation="zoomInLeft" <?php echo 'style="background-image: url('.esc_url($additional_img).');"' ?>></div>
            <?php endif ?>
            <?php if (!empty($product_img)): ?>
            	<div class="image-add cell-section text-center element-animate" data-animation="flipInX">
            	    <img src="<?php echo esc_url($product_img); ?>"  alt="Add Image">
            	</div>
            <?php endif ?>
            <div class="add-content cell-section element-animate" data-animation="zoomInRight">
                <?php if (!empty($price)): ?>
                	<div class="hidden-xs col-sm-4 col-md-4 col-lg-4 omega alpha">
                	    <div class="pricing-tag">
                	        <div class="border-rounded additional-border border-white"></div>
                	        <?php echo $price; ?>
                	    </div>
                	</div>
                <?php endif ?>
                <?php if (!empty($description)): ?>
                	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 omega alpha">
                	    <div>
                	        <p><?php echo $description; ?></p>
                	    </div>
                	</div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<!-- End Addvertisement Item -->
